package com.hegedusviktor;

public class MaxFindThread extends Thread {

    public int[] array;
    public int from;
    public int to;
    public int MAX_VALUE = Integer.MIN_VALUE;

    @Override
    public void run() {
        System.out.println("Starting #" + Thread.currentThread().getName() + " ...");
        for (int i = from; i < to; i++) {
            if (array[i] > MAX_VALUE) {
                MAX_VALUE = array[i];
            }
        }
        System.out.println("Max value in thread #" + Thread.currentThread().getName() + " is " + MAX_VALUE);
    }
}
