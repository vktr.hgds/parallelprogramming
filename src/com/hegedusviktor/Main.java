package com.hegedusviktor;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int SIZE = 200000000; //size of the array
        long start = System.currentTimeMillis(); //start counting the milliseconds

        int[] mainArray = new int[SIZE];
        mainArray = generateArray(mainArray);
        int maxInMain = findMax(mainArray);

        System.out.println("Main thread took " + (System.currentTimeMillis() - start) + " ms to find the maximum value"); //elapsed time

        List<Integer> maxVal = new ArrayList<Integer>(); //store each maximum values in an arraylist
        System.out.println("");

        long now = System.currentTimeMillis();
        int threads = 8; //number of threads

        for (int i = 0; i < threads; i++) { //start each thread

            MaxFindThread mt = new MaxFindThread();
            mt.from = (SIZE/threads) * i;
            mt.to = (SIZE/threads) * (i + 1);
            mt.array = mainArray;
            mt.start();

            try {
                mt.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            maxVal.add(mt.MAX_VALUE);
        }

        //find the (global) maximum in the arraylist
        int maxInList = Integer.MIN_VALUE;
        for (int i = 0; i < maxVal.size(); i++) {
            if (maxVal.get(i) > maxInList) {
                maxInList = maxVal.get(i);
            }
        }
        System.out.println("\nMax value is " + maxInList);

        /*
        if (maxInList == maxInMain) {
            System.out.println("\nSAME SOLUTION ...");
        }
        */

        System.out.println(maxVal.size() + " threads took " + (System.currentTimeMillis() - now) + " ms to find the maximum value");

    }

    private static int[] generateArray(int array[]) {
        Random rand = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt();
        }
        return array;
    }

    private static int findMax (int[] array) {
        int MAX_VALUE = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > MAX_VALUE) {
                MAX_VALUE = array[i];
            }
        }
        System.out.println("Max value in the MAIN THREAD: " + MAX_VALUE);
        return MAX_VALUE;
    }
}
